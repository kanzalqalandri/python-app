FROM python:3.10

WORKDIR /app

COPY requirements.txt .

RUN pip3 install -r requirements.txt

RUN pip3 install pymongo

COPY . .

EXPOSE 5000

# ENV VARIABLE_NAME=value

CMD ["python", "app.py"]

